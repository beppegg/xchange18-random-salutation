package com.reply.xchange18.ocrandomsalutation.web;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.concurrent.ThreadLocalRandom;

@RestController
@RequestMapping("/salutation")
public class SalutationController {

	private static final String[] greets = new String[]{"Hello", "Hi", "Ciao", "Salut", "Guten tag"};

	@PostMapping
	public Mono<String> salutationGenerator() {
		return Mono.just(greets[ThreadLocalRandom.current().nextInt(greets.length)]);
	}
}
