package com.reply.xchange18.ocrandomsalutation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OcRandomSalutationApplication {

	public static void main(String[] args) {
		SpringApplication.run(OcRandomSalutationApplication.class, args);
	}
}
